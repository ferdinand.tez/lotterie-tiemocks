# Lotterie timelock
Ce projet accompagne un projet de création de lotterie en Michelson utilisant des timelocks.

## Requirements
Ce projet nécessite octez connecté à un réseau tezos utilisant au minimum le protocole Oxford.

Une adresse avec des tez (on l'appellera _address_ dans la suite du document, elle désigne l'adresse au format B58 ou être un alias. 
## Connexion à une lotterie existante
Pour participer, vous devez avoir des tez sur ce réseau (au moins la valeur du minimum\_ammount



```sh
. ./lotterie.sh
 register_contract KT1...
 participate address # Si vous voulez mettre plus de tez, vous pouvez ajouter un montant (en ꜩ)
 ```
 Vous n'avez plus qu'à attendre
 
 
## Création d'une nouvelle lotterie
Pour créer une nouvelle lotterie, il faut commencer
 ```sh
 archetype lotterie.arl > lotterie.tz
 . lotterie.sh
 originate_contract from address
 ```
 
 La lotterie est prête. Une fois terminée (lorsque la lotterie a été remportée ou si elle n'a pas été utilisée), il suffit de faire `restart lotterie` pour la relancer (Attention sa balance doit être égale à 0).

## Changer la durée de la lotterie
Pour changer la durée des timelocks, il faut changer à la fois la difficulté et le délai dans les deux fichiers (lotterie.sh et lotterie.arl) de manière cohérente.
Selon les analyses, une difficulté de 1000000000 correspond à une durée d'environ 1h. Il est conseillé de faire un étalonage en faisant `time build_timelock_generator n` (n étant la difficulté à analyser).

Une fois que le générateur est construit, il peut être transmis aux participants qui peuvent l'utiliser en le mettant dans le dossier timelock 
