archetype lotterie


variable end_participation : date = 2019-01-01
variable unlock_opening : date = 2019-01-01

constant timelock_time : duration = 1h
constant register_minimal_time : duration = 5m
constant difficulty : nat = 1_000_000_000
constant minimal_tez_amount : nat = 100
constant minimal_amount : tez = 100*1tz

asset participations {
      min_value : tez;
      max_value : tez;
      owner : or<address * chest,address>;
}

record unlocker {
      recorded_hash : option<bytes>;
      valid_record_time : date = now + timelock_time;
}

asset reverse_participations{
      timelock_hash : bytes;
      participation_id : tez;
      range : tez = transferred;
      unlock_records : nat = 0;
      unlockers : map<address,unlocker> = make_map<address,unlocker>([])
}

variable random_value : bytes = 0x
variable locks : int = 0

entry participate(participation: chest){
      require {
        participate_r1: now < end_participation otherwise "PARTICIPATION_PERIOD_IS_OVER";
	participate_r2: transferred >= minimal_amount otherwise (("The minimal participation is fixed at",minimal_tez_amount,"XTZ"))
      }
      
      effect {
        participations.add_update(balance - transferred, {max_value = balance; owner = left<address>((caller,participation))});
	reverse_participations.add({timelock_hash = sha256(pack(participation));participation_id = (balance - transferred)});
	locks := locks + 1;
     }
}

entry reveal(val: bytes, reveal_key : chest_key){
      no transfer
      require{
	reveal_r1: end_participation < now;
      }
      effect {
	var id = reverse_participations[val].participation_id;
	reverse_participations.remove(val);
	var participation = participations[id].owner;
	match participation with
	    | right(s) -> fail("Already unlock")
	    | left(a)  -> do_require(a[0] = caller,"Only originator can call this entrypoint");
	      var timelock = a[1];
	      var p ?= open_chest(reveal_key, timelock, difficulty);
	      random_value := random_value xor p;
	      participations.update(id,{owner = right<address * chest>(caller)});
	      locks := locks -1
	end;
      }
     }

entry record_unlock(val : bytes){
      no transfer
      require{
          record_unlock_r1: transferred = 0tz;
      }
      effect {
      	    reverse_participations[val].unlockers.put(caller, {recorded_hash = none});
	reverse_participations[val].unlock_records += 1
      }
}

entry pre_reveal_unlock(val : bytes, hash : bytes){
      no transfer
      require{
	pre_reveal_unlock_r1: map(reverse_participations[val].unlockers[caller], x -> x.valid_record_time < now) = some(true);
	pre_reveal_unlock_r2: map(reverse_participations[val].unlockers[caller], x -> x.recorded_hash) = some(none<bytes>)
      }
      effect {
      	reverse_participations[val].unlockers.put(caller,{recorded_hash = some(hash); valid_record_time = (now + register_minimal_time)})
      }
}

entry reveal_unlock(val : bytes, reveal_key : chest_key * address){
      no transfer
      require{
	reveal_unlock_r1: map(reverse_participations[val].unlockers[caller], x -> x.valid_record_time < now) = some(true);
	reveal_unlock_r2: map(reverse_participations[val].unlockers[caller], x -> x.recorded_hash) = some(some(sha256(pack(reveal_key))))
      }
      effect {
      	var id = reverse_participations[val].participation_id;
	reverse_participations.remove(val);
	var participation = participations[id].owner;
	match participation with
	    | right(s) -> fail("Already unlock")
	    | left(a)  -> var timelock = a[1];
	      var p ?= open_chest(reveal_key[0], timelock, difficulty);
	      random_value := random_value xor p;
	      participations.update(id,{owner = right<address * chest>(reveal_key[1])});
       	      locks := locks -1
	end;
      }
}

entry unlock(val: bytes, reveal_key : chest_key){
      no transfer
      require{
	unlock_r1: unlock_opening < now;
      }
      effect {
      	var id = reverse_participations[val].participation_id;
	reverse_participations.remove(val);
	var participation = participations[id].owner;
	match participation with
	    | right(s) -> fail("Already unlock")
	    | left(a)  -> var timelock = a[1];
	      var p ?= open_chest(reveal_key, timelock, difficulty);
	      random_value := random_value xor p;
	      participations.update(id,{owner = right<address * chest>(caller)});
       	      locks := locks -1
	end;
      }
    }

/*
function calc_final_random_value(b : bytes, amount : tez): tez{
	 var num = (bytes_to_nat(b) * 0.000001tz) % amount;
	 return num
	 
}
*/

function calc_final_random_value(b : bytes, amount : tez): tez{
	 return michelson<tez>
	 { NAT;
	   SWAP;
           PUSH mutez 1;
	   SWAP;
           EDIV;
	   ASSERT_SOME;
	   CAR;
	   SWAP;
	   EDIV;
	   ASSERT_SOME;
	   CDR;
	   PUSH mutez 1;
	   MUL}
	 [amount : b]
}

entry claim(proof : tez){
      no transfer
      constant{
	final_random_value is calc_final_random_value(random_value,balance);
      }
      require{
	claim_r1: locks = 0;
	claim_r2: final_random_value >= proof;
	claum_r3: final_random_value < participations[proof].max_value
      }
      effect {
	match participations[proof].owner with
	    | left(s) -> fail("Assertion failure : not unlocked value")
	    | right(a) -> transfer balance to a
	end;
        participations.remove_all()
      }
}

entry restart(){
      no transfer
      require{
	restart_r1: balance = 0tz
      }
      effect {      
      	end_participation := now + timelock_time;
	unlock_opening := end_participation + timelock_time
      }
}

view not_unlocked_timelock() : option<chest> {
     var nb_records : nat = 0;
     var val_range = 0tz;
     var best_timelock : tez = balance;
     for participation in reverse_participations do
     	 if reverse_participations[participation].unlock_records < nb_records
	 or reverse_participations[participation].unlock_records = nb_records and reverse_participations[participation].range > val_range
	 or best_timelock = balance
	 then begin
	     nb_records := reverse_participations[participation].unlock_records;
	     val_range := reverse_participations[participation].range;
	     best_timelock := reverse_participations[participation].participation_id
	 end;
     done;
     return
	(if best_timelock = balance then
	    none<chest>
	 else begin
     	 match participations[best_timelock].owner with
	    | right(s) -> none<chest>
	    | left(s) -> some(s[1])
	 end;
	 end)
     }

view winner() : address * tez{
     var v = calc_final_random_value(random_value,balance);
     var winner_id = balance;
     for participant in participations do
     	 if participant < v then
	    winner_id := participant
     done;
     var winner_address =
     	 (match participations[winner_id].owner with
     	     | left(a) -> a[0]
	     | right(a) -> a
	 end);
     return ((winner_address, winner_id))
}

view start_of_reveals() : date {
     return end_participation
}

view remaining_time_to_participate() : duration {
     return min(0s , end_participation - now)
}