# set constants
difficulty=1000000000
locked_time=$((60 * 60))
prereveal_time=$((5 * 60))
minimal_amount=100
alias register_contract='octez-client remember contract lotterie'

restart(){
    octez-client call $1 from $3 --entrypoint restart --burn-cap 0.002
}

originate_contract(){
    # origination du contrat
    octez-client originate contract lotterie transferring 0 from $2 running lotterie.tz --init '(Pair 0 0 {} {} 0x 0)' --burn-cap 2
    ## démarage de la première lotterie
    restart lotterie from $2
}

build_timelock_generator(){
    mkdir -p timelocks;
    octez-client timelock precompute for ${1:-$difficulty} in timelocks
}

sleeep(){ m=$1; for i in $(seq $1); do echo -en "$((m - i)) \r"; sleep 1; done }

send_timelock(){
     ## Génération de la variable
    self_address=$(octez-client list known addresses | grep $1 | sed -e 's/[^ ]* \([^ ]*\) .*$/\1/g')
    random_value=$(octez-client run michelson code BYTES on stack "{ Stack_elt nat $(for i in {1..12}; do date +%N; done | tr '\n' ' ' | sed -e 's/ //g') }" | grep "  0x.* }" | sed -e 's/[^0-9a-fx]//g')

    # Creation du timelock
    mkdir timelocks/timelock_for_$random_value
    cp timelocks/*_${difficulty}.json timelocks/timelock_for_$random_value
    octez-client timelock create for $difficulty with $random_value in timelocks/timelock_for_$random_value
    timelock=0x$(cat timelocks/timelock_for_$random_value/time_chest_* | xxd -p -c 0 | tr -d '\n')
    timelock_key=0x$(cat timelocks/timelock_for_$random_value/time_key_create_* | xxd -p -c 0 | tr -d '\n')
    timelock_hash=$(octez-client hash data $timelock of type bytes | grep "^Raw Sha256 hash: " | sed -e 's/^Raw Sha256 hash: //g')
}

reveal_key(){
    sleeep $(octez-client run view remaining_time_to_participate on contract lotterie)
    octez-client call lotterie from $self_address --entrypoint reveal --arg '(Pair '$timelock_hash' '$timelock_key')' && rm -r timelocks/timelock_for_$random_value
}


solve_timelocks(){
    self_address=$1
    timelock_to_solve=$2
    timelock_to_solve_hash=$3
    
    echo $timelock_to_solve | xxd  -p -r > timelocks/key_for_$timelock_to_solve_hash/timelock
    octez-client call lotterie from $1 --entrypoint record_unlock --arg $timelock_to_solve_hash --burn-cap 1 &
    octez-client timelock open for $difficulty chest timelocks/key_for_$timelock_to_solve_hash/timelock in timelocks/key_for_$timelock_to_solve_hash/ &
    sleeep $locked_time && wait
    timelock_solved_key=0x$(cat timelocks/key_for_$timelock_to_solve_hash/time_key_open_* | xxd -p -c 0 | tr -d '\n')
    pre_reveal=$(octez-client hash data 'Pair '$timelock_solved_key' "'$self_address'"' of type 'pair chest_key address' | grep "^Raw Sha256 hash: " | sed -e 's/^Raw Sha256 hash: //g')
    octez-client call lotterie from $1 --entrypoint pre_reveal_unlock --arg '(Pair '$timelock_to_solve_hash' '$pre_reveal')' && sleeep $prereveal_time &&
    octez-client call lotterie from $1 --entrypoint reveal_unlock --arg  '(Pair '$timelock_to_solve_hash' (Pair '$timelock_solved_key' "'$self_address'"))';
	    
    rm -r timelocks/key_for_$timelock_to_solve_hash/
}

claim(){
    octez-client call lotterie from $self_address --entrypoint claim --arg "$(octez-client run view winner on contract lotterie| sed -e 's/.* \([0-9]*\))/\1/g')"
    }

participate(){
    ## Génération de la variable
    self_address=$(octez-client list known addresses | grep $1 | sed -e 's/[^ ]* \([^ ]*\) .*$/\1/g')
    random_value=$(octez-client run michelson code BYTES on stack "{ Stack_elt nat $(for i in {1..12}; do date +%N; done | tr '\n' ' ' | sed -e 's/ //g') }" | grep "  0x.* }" | sed -e 's/[^0-9a-fx]//g')

    # Creation du timelock
    mkdir timelocks/timelock_for_$random_value
    cp timelocks/*_${difficulty}.json timelocks/timelock_for_$random_value
    octez-client timelock create for $difficulty with $random_value in timelocks/timelock_for_$random_value
    timelock=0x$(cat timelocks/timelock_for_$random_value/time_chest_* | xxd -p -c 0 | tr -d '\n')
    timelock_key=0x$(cat timelocks/timelock_for_$random_value/time_key_create_* | xxd -p -c 0 | tr -d '\n')
    timelock_hash=$(octez-client hash data $timelock of type bytes | grep "^Raw Sha256 hash: " | sed -e 's/^Raw Sha256 hash: //g')

    # Envoi du timelock
    octez-client transfer ${2:-$minimal_amount} from $1 to lotterie --entrypoint participate --arg $timelock --burn-cap 1 

    sleeep $locked_time
    
    # revelation du timelock
    octez-client call lotterie from $1 --entrypoint reveal --arg '(Pair '$timelock_hash' '$timelock_key')' && rm -r timelocks/timelock_for_$random_value

    # On regarde si il existe des timelocks non résolus
    unreveled_timelock=$(octez-client run view not_unlocked_timelock on contract lotterie);
    while [[ $unreveled_timelock != None ]]; do
	timelock_to_solve=$(echo $unreveled_timelock | sed 's/^(Some \(0x[0-9a-f]*\))/\1/g');
	timelock_to_solve_hash=$(octez-client hash data $timelock_to_solve of type chest | grep "^Raw Sha256 hash: " | sed -e 's/^Raw Sha256 hash: //g')

	mkdir timelocks/key_for_$timelock_to_solve_hash 2> /dev/null && solve_timelocks $self_address $timelock_to_solve $timelock_to_solve_hash
	unreveled_timelock=$(octez-client run view not_unlocked_timelock on contract lotterie);
    done
    # calcul du gagnant
    winner=$(octez-client run view winner on contract lotterie | sed -e 's/(Pair "\(.*\)" [0-9]*)/\1/g')
    self_address=$(octez-client list known addresses | grep $1 | sed -e 's/[^ ]* \([^ ]*\) .*$/\1/g')		   

    # appel du point d'entrée
    if [[ $winner == $self_address ]]; then
       octez-client call lotterie from $1 --entrypoint claim --arg "$(octez-client run view winner on contract lotterie| sed -e 's/.* \([0-9]*\))/\1/g')"
    fi
}
